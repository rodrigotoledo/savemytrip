import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Home from '~/screens/Home';
import SignIn from '~/screens/SignIn';
import SignUp from '~/screens/SignUp';

const Routes = createAppContainer(
  createSwitchNavigator({
    Home,
    SignUp,
    SignIn
    }
  )
);

export default Routes;
