import React, {Component} from 'react';
import { Provider } from 'react-redux';
import '~/config/DevToolsConfig';
import '~/config/ReactotronConfig';
import Routes from '~/routes';
import store from '~/store';



const App = () => (
  <Provider store={store}>
    <Routes />
  </Provider>
);

export default App;
