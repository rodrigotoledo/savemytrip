import { StyleSheet } from 'react-native';

const GeneralStyles = StyleSheet.create({
  ViewBackGround: {flex: 1, alignItems: 'center', justifyContent: 'center'},
  TextInput: { marginTop: 30, backgroundColor: 'rgba(255, 255, 255, 0.8)', width: '90%', borderRadius: 10, paddingLeft: 10, paddingRight: 10, justifyContent: 'center' },
  ButtonFooter: {backgroundColor: '#314855', borderRadius: 5, padding: 10, alignItems: 'center', justifyContent: 'center', margin: 10},
  FooterBackground: { backgroundColor: 'rgba(255, 255, 255, 0.4)', alignItems: 'center', justifyContent: 'center', width: '100%', flexDirection: 'row' }
});

export default GeneralStyles;
