export default {
  white: '#fff',
  lighter: '#eee',
  light: '#ddd',
  regular: '#999',
  dark: '#666',
  darker: '#333',
  black: '#000',
}
