
import React, { Component } from 'react';
import { View, Text } from 'react-native';
import colors from '~/styles/colors';
import Geolocation from 'react-native-geolocation-service';

export default class Header extends Component {
  componentWillMount = () => {
    // Instead of navigator.geolocation, just use Geolocation.
      Geolocation.getCurrentPosition(
        (position) => {
            console.tron.log(position);
        },
        (error) => {
            // See error code charts below.
            console.tron.log(error.code, error.message);
        },
        { enableHighAccuracy: true, timeout: 15000, maximumAge: 10000 }
      );
  }
  render() {
    return (
      <View>
        <Text style={{color: colors.white}}>Minha localização atual: 0.00000,0.000000</Text>
      </View>
    )
  }
}
