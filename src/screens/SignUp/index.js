import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { Text, View, ImageBackground, Image, TouchableWithoutFeedback, TextInput } from 'react-native';
import Header from '~/components/Header';

import * as UserActions from '~/store/actions/users';
import GeneralStyles from '~/styles/general';
import { Container } from '~/styles/metrics';

export class SignUp extends Component {
  state = {
    user: {}
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={require('~/assets/background.png')}
          imageStyle={{ resizeMode: 'stretch' }}
          style={GeneralStyles.ViewBackGround}>
          <Header />
          <View style={GeneralStyles.ViewBackGround}>
            <Image source={require('~/assets/logo.png')} />

            <View style={GeneralStyles.TextInput}>
              <TextInput placeholder='Who are you? Put your name here' style={{fontSize: 18, color: '#747474'}} />
            </View>

            <View style={GeneralStyles.TextInput}>
              <TextInput keyboardType='email-address' placeholder='and what is your email... write here' style={{fontSize: 18, color: '#747474'}} />
            </View>

            <View style={GeneralStyles.TextInput}>
              <TextInput secureTextEntry={true} placeholder='of course, password too' style={{fontSize: 18, color: '#747474'}} />
            </View>

            <View style={GeneralStyles.TextInput}>
              <TextInput secureTextEntry={true} placeholder='just confirm the password' style={{fontSize: 18, color: '#747474'}} />
            </View>

            <TouchableWithoutFeedback>
              <View style={{marginTop: 30, backgroundColor: '#314855', borderRadius: 5, padding: 10, alignItems: 'center', justifyContent: 'center'}}>
                <Text style={{fontSize: 18, color: '#fff'}}>Sign Up</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
          <View style={GeneralStyles.FooterBackground}>
            <TouchableWithoutFeedback>
              <View style={GeneralStyles.ButtonFooter}>
                <Text style={{fontSize: 18, color: '#fff'}}>Save me Now!</Text>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback>
              <View style={GeneralStyles.ButtonFooter}>
                <Text style={{fontSize: 18, color: '#fff'}}>Sign In</Text>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </ImageBackground>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
