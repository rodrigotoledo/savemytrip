import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';

import { View, ImageBackground, Image, TouchableWithoutFeedback } from 'react-native';
import Header from '~/components/Header';

import * as UserActions from '~/store/actions/users';
import GeneralStyles from '~/styles/general';
import { Container } from '~/styles/metrics';
import { ViewButtonSaveMeNow, ButtonSaveMeNow, ButtonTextCommon } from './styles';


export class Home extends Component {
  state = {
    user: {}
  }

  render() {
    return (
      <Container>
        <ImageBackground
          source={require('~/assets/background.png')}
          imageStyle={{ resizeMode: 'stretch' }}
          style={GeneralStyles.ViewBackGround}>
          <Header />
          <View style={GeneralStyles.ViewBackGround}>
            <Image source={require('~/assets/logo.png')} />

            <ViewButtonSaveMeNow>
              <ButtonSaveMeNow>
                <ButtonTextCommon>Just Save me NOW!</ButtonTextCommon>
              </ButtonSaveMeNow>
            </ViewButtonSaveMeNow>
          </View>
          <View style={GeneralStyles.FooterBackground}>
            <TouchableWithoutFeedback>
              <View style={GeneralStyles.ButtonFooter}>
                <ButtonTextCommon>Sign In</ButtonTextCommon>
              </View>
            </TouchableWithoutFeedback>

            <TouchableWithoutFeedback>
              <View style={GeneralStyles.ButtonFooter}>
                <ButtonTextCommon>Sign Up</ButtonTextCommon>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </ImageBackground>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  user: state.user
})

const mapDispatchToProps = dispatch =>
  bindActionCreators(UserActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Home);
