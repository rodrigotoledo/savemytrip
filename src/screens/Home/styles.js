import styled from 'styled-components/native';

export const ViewButtonSaveMeNow = styled.View`
  margin-top: 30px;
  background-color: #314855;
  padding: 20px;
  border-radius: 20px;
  align-items: center;
  justify-content: center
`;

export const ButtonSaveMeNow = styled.TouchableWithoutFeedback`
  margin-top: 30px;
  padding: 20px;
  border-radius: 50px;
  align-items: center;
  justify-content: center
`;

export const ButtonTextCommon = styled.Text`
  font-size: 18px;
  color: #d3d3d3;
`;
